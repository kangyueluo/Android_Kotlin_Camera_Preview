package com.ar.kangyur.android_kart

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.graphics.PixelFormat
import android.hardware.Camera
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import com.anthonycr.grant.PermissionsManager
import com.anthonycr.grant.PermissionsResultAction

class CameraActivity : AppCompatActivity(),SurfaceHolder.Callback, Camera.PreviewCallback {



    private val TAG : String = "CameraActivity"
    private lateinit var cameraView : SurfaceView
    private lateinit var transparentView : SurfaceView
    private lateinit var holderTransparent : SurfaceHolder
    private lateinit var holderCamera : SurfaceHolder
    private var camera : Camera ? = null
    private val permissions = arrayOf(Manifest.permission.CAMERA)
    internal var frameWidth = 0
    internal var frameHeight = 0
    internal var isCameraPreview = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d(TAG,"onCreate")

        setOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setWindowFeature(Window.FEATURE_NO_TITLE)
        setFullScreen()
        setContentView(R.layout.activity_main)
        askPermission(this,permissions)
        initView()

    }

    private fun setOrientation(orietation:Int){
        setRequestedOrientation(orietation)
    }

    private fun setWindowFeature(feature:Int){
        requestWindowFeature(feature)
    }
    private fun setFullScreen(){
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }
    private fun initView(){
        cameraView = findViewById(R.id.CameraView)
        holderCamera = cameraView.holder
        holderCamera.addCallback(this as SurfaceHolder.Callback)
        cameraView.setSecure(true)

        transparentView = findViewById(R.id.TransparentView) as SurfaceView
        holderTransparent = transparentView.holder
        holderTransparent.addCallback(this as SurfaceHolder.Callback)
        holderTransparent.setFormat(PixelFormat.TRANSLUCENT)
        transparentView.setZOrderMediaOverlay(true)

    }

    private fun askPermission(activity: Activity, permissions: Array<String>) {
        PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(activity,
                permissions, object : PermissionsResultAction() {

            override fun onGranted() {
                for (i in permissions) {
                    Log.d(TAG, i + " Granted")

                }
            }

            override fun onDenied(permission: String) {
                for (i in permissions) {
                    Log.d(TAG, i + " Denied")
                }
            }
        })
    }

    override fun surfaceCreated(surfaceHolder: SurfaceHolder?) {

        try {

            camera = Camera.open() //open a camera

            val size = camera!!.getParameters().previewSize

            frameWidth = size.width
            frameHeight = size.height

            Log.d(TAG, "frameWidth :$frameWidth")
            Log.d(TAG, "frameHeight :$frameHeight")

        } catch (e: Exception) {

            Log.i("Exception", e.toString())

            return
        }


        val param: Camera.Parameters

        param = camera!!.getParameters()

        val display = (getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay

        if (display.rotation == Surface.ROTATION_0) {
            camera!!.setDisplayOrientation(90)
        }
        camera!!.setParameters(param)

        try {
            camera!!.setPreviewDisplay(holderCamera)

            camera!!.startPreview()
            isCameraPreview = true
        } catch (e: Exception) {
            return
        }
    }

    override fun onPreviewFrame(p0: ByteArray?, p1: Camera?) {

    }

    override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {
        refreshCamera() //call method for refress camera
    }

    override fun surfaceDestroyed(p0: SurfaceHolder?) {

    }

    fun refreshCamera() {

        if (holderCamera.getSurface() == null) {

            return
        }

        try {
            camera!!.stopPreview()
            isCameraPreview = false

        } catch (e: Exception) {

        }

        try {
            camera!!.setPreviewDisplay(holderCamera)
            camera!!.setPreviewCallback(this)
            camera!!.startPreview()
            isCameraPreview = true

        } catch (e: Exception) {

        }

    }


    override fun onDestroy() {
        super.onDestroy()
        // 釋放 相機
        holderCamera.removeCallback(this)
        holderTransparent.removeCallback(this)
        holderCamera.removeCallback(this)
        camera = null
    }

}


